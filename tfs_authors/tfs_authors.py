import json
from tfs import TFSAPI
from requests import get
from requests_ntlm import HttpNtlmAuth

class TfsAuthors():
    """
        Python class to generate an authors file for use
        with the git-tfs(https://github.com/git-tfs/git-tf) library
    """
    def __init__(self, url, project, user, password):
        self.user = user
        self.password = password
        self.client = TFSAPI(url, project=project, user=user, password=password, auth_type=HttpNtlmAuth)
        self.identity_map = {}
        self.paginated = False
        self.to_ = -1
        self.from_ = -1
        self.authors_list = []
    
    def append_to_identity_map(self, response):
        """
            Appends data to identity map

            Using a map to save lookup times
        """
        self.increment_pagination(response[-1].changesetId)
        self.last_id = response[-1].changesetId
        print(f"Retrieving data from {self.from_} to {self.to_}")
        for cs in response:
            author = cs["author"]
            if not self.identity_map.get(author["id"]):
                print(f"- Storing author {author['displayName']} in identity map")
                self.identity_map[cs["author"]["id"]] = author
                self.append_to_authors_list(author)
    
    def append_to_authors_list(self, author):
        """
            Appends the actual author file entry to self.authors_list

            A second request is required to get the user's email,
            which we are assuming will be the same on the git side
        """
        author_resp = get(author["url"], auth=HttpNtlmAuth(self.user, self.password)).json()
        author_email = author_resp["Properties"].get("Mail")
        if author_email:
            self.authors_list.append(f"{author['uniqueName']} = {author['displayName']} <{author_email}>")
    
    def increment_pagination(self, changeset_id):
        """
            Increments the changeset API pagination.
            Max size is 256 results per request
        """
        self.to_ = changeset_id - 1
        self.from_ = changeset_id - 257 if changeset_id > 257 else 0

    def generate_authors_file(self):
        """
            Iterates over the TFS changeset API to populate
            self.identity_map and self.authors_list
        """
        while True:
            if self.from_ != 0:
                if self.paginated:
                    self.append_to_identity_map(self.client.get_changesets(to_=self.to_, from_=self.from_))
                else:
                    self.append_to_identity_map(self.client.get_changesets())
                    self.paginated = True
            else:
                break
    
    def write_authors_file(self, path='output.txt'):
        """
            Writes self.authors_list to a file
        """
        with open(path, "w") as f:
            for author in self.authors_list:
                f.write(f"{author}\n")
