"""
TFS Authors - Generate TFS authors file for git-tfs

Copyright (c) 2021 - GitLab

Usage:
    tfs-authors generate (--user=<user> --password=<password> --url=<url> --project=<project>) [--file-path=<path>]

Options:
    -h, --help  Show usage

Arguments:
    user        TFS Username
    password    TFS User password
    url         TFS instance URL (ex: https://company.example.com:8080/tfs)
    project     TFS project/collection path (ex: DefaultCollection/Project)
    file-path   File path for storing generated authors file. Defaults to './output.txt'

Commands:
    generate    Generate the authors file
"""

from docopt import docopt
from tfs_authors.tfs_authors import TfsAuthors

def main():
    arguments = docopt(__doc__)
    user = arguments['--user']
    password = arguments['--password']
    url = arguments['--url']
    project = arguments['--project']
    file_path = arguments['--file-path']

    if arguments['generate']:
        client = TfsAuthors(url, project, user, password)
        client.generate_authors_file()
        if file_path:
            client.write_authors_file(path=file_path)
        else:
            client.write_authors_file()