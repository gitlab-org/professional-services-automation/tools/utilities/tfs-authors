# TFS Authors

Retrieve authors from a TFS collection

## Setup

```bash
git clone <this repo>
poetry install <this cloned repo>
```

## Usage:

```python
from tfs_authors.tfs_authors import TfsAuthors

user = "user.name"
password = "password"
url = "http://tfs:8080/tfs/"
project = "DefaultCollection/Project"

# Initialize TfsAuthors class
client = TfsAuthors(url, project, user, password)

# Generate the data
client.generate_authors_file()

# Write the authors list to a file
client.write_authors_file()
# Specify a file name/path
client.write_authors_file(path='project_authors.txt')
```